init();

let hueApiUrl = '';

function init() {
    const getItem = browser.storage.local.get('hueApiUrl');
    getItem.then(onGot, onError);
}


function connectToHue() {
    if (!(hueApiUrl.length > 0)) {
        const req = new XMLHttpRequest();
        req.addEventListener('load', hueListener);
        req.open('GET', 'https://discovery.meethue.com/');
        req.send();
    }
}


function hueListener() {
    const response = JSON.parse(this.responseText);
    const hueIp = response[0].internalipaddress;
    registerHueApiUser(hueIp);
}

function registerHueApiUser(hueIp) {
    const req = new XMLHttpRequest();
    req.addEventListener('load', function () {
        getHueApiUser(this.responseText, hueIp);
    });
    req.open('POST', 'http://' + hueIp + '/api');
    req.setRequestHeader('Content-Type', 'application/json');
    req.send(JSON.stringify({ devicetype: 'hue-ext' }));
}

function getHueApiUser(responseText, ip) {
    const response = JSON.parse(responseText);
    if (response[0].success) {
        const apiUrl = `http://${ip}/api/${response[0].success.username}`;
        hueApiUrl = apiUrl;
        browser.storage.local.set({
            hueApiUrl: apiUrl
        });
        const connectButton = document.querySelector('#connectButton');
        connectButton.parentNode.removeChild(connectButton);
        fetchLights(apiUrl);
    }
}

function fetchLights(apiUrl) {
    const req = new XMLHttpRequest();
    req.addEventListener('load', getLights);
    req.open('GET', `${apiUrl}/lights`);
    req.send();
}

function onGot(item) {
    if (item.hueApiUrl) {
        hueApiUrl = item.hueApiUrl;
        const req = new XMLHttpRequest();
        req.addEventListener('load', getLights);
        req.open('GET', `${hueApiUrl}/lights`);
        req.send();
    } else {
        const buttonContainer = document.querySelector('#button-container');
        const connectButton = document.createElement('button');
        connectButton.id = 'connectButton';
        connectButton.innerText = 'Connect to Hue';
        connectButton.addEventListener('click', connectToHue)
        buttonContainer.appendChild(connectButton);
    }
}

function getLights() {
    const response = JSON.parse(this.responseText);
    const lightContainer = document.querySelector('#light-container');
    for (const [key, value] of Object.entries(response)) {
        const lightElement = document.createElement('div');
        const nameSpan = document.createTextNode(value.name);
        lightElement.appendChild(nameSpan);

        const onButton = document.createElement('button');
        onButton.innerText = 'Turn Light on';
        onButton.addEventListener('click', turnLightOn);
        onButton.id = key;

        const offButton = document.createElement('button');
        offButton.innerText = 'Turn light off';
        offButton.id = key;

        offButton.addEventListener('click', turnLightOff);

        lightElement.appendChild(onButton);
        lightElement.appendChild(offButton);

        lightContainer.appendChild(lightElement);
    }
}

function turnLightOff() {
    if (hueApiUrl.length > 0) {
        const req = new XMLHttpRequest();
        req.addEventListener('load', getPut);
        req.open('PUT', `${hueApiUrl}/lights/${this.id}/state`);
        req.setRequestHeader('Content-Type', 'application/json');
        req.send(JSON.stringify({ on: false }));
    }
}

function turnLightOn() {
    if (hueApiUrl.length > 0) {
        const req = new XMLHttpRequest();
        req.addEventListener('load', getPut);
        req.open('PUT', `${hueApiUrl}/lights/${this.id}/state`);
        req.setRequestHeader('Content-Type', 'application/json');
        req.send(JSON.stringify({ on: true }));
    }
}

function getPut() {
    const response = JSON.parse(this.responseText);
}

function onError(error) {
    console.log(`Error: ${error}`);
}